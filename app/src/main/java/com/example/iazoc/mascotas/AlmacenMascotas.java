package com.example.iazoc.mascotas;

import com.example.iazoc.mascotas.Clase.Mascota;

import java.util.ArrayList;

/**
 * Created by iazoc on 17-04-2017.
 */

public class AlmacenMascotas {

    private static ArrayList<Mascota> mascotas = new ArrayList<>();

    public static void agregarFormulario(Mascota mascota){
        mascotas.add(mascota);
    }

    public static ArrayList<Mascota> getFormularios(){
        return mascotas;
    }

}
