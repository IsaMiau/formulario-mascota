package com.example.iazoc.mascotas.Clase;

/**
 * Created by iazoc on 17-04-2017.
 */

public class TipoAnimal {
    private String texto;

    public TipoAnimal() {}

    public TipoAnimal(String texto) {
        setTexto(texto);
    }


    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String toString() {
        return texto;
    }
}
