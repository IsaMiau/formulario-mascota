package com.example.iazoc.mascotas.Clase;

/**
 * Created by iazoc on 17-04-2017.
 */

public class Mascota {
    private String nombre;
    private String tipo;
    private String fecha_nac;
    private String edad;
    private String nombreDueno;
    private String urlImagen;

   public Mascota() {
    }

    public Mascota(String nombre, String tipo, String fecha_nac, String edad, String nombreDueno, String urlImagen) {
        setNombre(nombre);
        setTipo(tipo);
        setFecha_nac(fecha_nac);
        setEdad(edad);
        setNombreDueno(nombreDueno);
        setUrlImagen(urlImagen);
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setFecha_nac(String fecha_nac) {
        this.fecha_nac = fecha_nac;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public void setNombreDueno(String nombreDueno) {
        this.nombreDueno = nombreDueno;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public String getFecha_nac() {
        return fecha_nac;
    }

    public String getEdad() {
        return edad;
    }

    public String getNombreDueno() {
        return nombreDueno;
    }

    public String getUrlImagen() { return urlImagen; }

    public void setUrlImagen(String urlImagen) {  this.urlImagen = urlImagen;  }
}
