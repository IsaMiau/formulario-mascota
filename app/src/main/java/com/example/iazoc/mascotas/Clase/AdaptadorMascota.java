package com.example.iazoc.mascotas.Clase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.iazoc.mascotas.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by iazoc on 17-04-2017.
 */

public class AdaptadorMascota extends ArrayAdapter<Mascota> {

    private ArrayList<Mascota> dataSource;

    public AdaptadorMascota(Context context, ArrayList<Mascota> dataSource){
        super(context, R.layout.activity_lista, dataSource);
        this.dataSource = dataSource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.activity_lista, null);

        TextView tvNombre = (TextView) item.findViewById(R.id.tvNombre);
        tvNombre.setText(dataSource.get(position).getNombre());

        TextView tvDueno = (TextView)item.findViewById(R.id.tvDueno);
        tvDueno.setText(dataSource.get(position).getNombreDueno());

        TextView tvTipo = (TextView)item.findViewById(R.id.tvTipo);
        tvTipo.setText(dataSource.get(position).getTipo());

        TextView tvEdad = (TextView)item.findViewById(R.id.tvEdad);
        tvEdad.setText(dataSource.get(position).getEdad());

        TextView tvFecha = (TextView)item.findViewById(R.id.tvFecha);
        tvFecha.setText(dataSource.get(position).getFecha_nac());


        ImageView ivPreview = (ImageView)item.findViewById(R.id.ivPreview);
        Picasso.with(getContext())
                .load(dataSource.get(position).getUrlImagen())
                .into(ivPreview);


        return(item);
    }

}
