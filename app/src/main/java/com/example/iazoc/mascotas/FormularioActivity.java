package com.example.iazoc.mascotas;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.iazoc.mascotas.Clase.Mascota;
import com.example.iazoc.mascotas.Clase.TipoAnimal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;

public class FormularioActivity extends AppCompatActivity {

    private EditText etUrlImagen;
    private ImageView ivPreview;
    private Button btnVerImagen;
    private Button btnFecha;
    private Spinner spnTipo;
    private EditText etNombre;
    private EditText etEdad;
    private EditText etDueno;
    private Button btnListar;


    private TextView txtEjemplo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        etUrlImagen = (EditText)findViewById(R.id.etUrlImagen);
        ivPreview = (ImageView)findViewById(R.id.ivPreview);
        btnVerImagen = (Button)findViewById(R.id.btnVerImagen);
        btnFecha = (Button)findViewById(R.id.btnFecha);
        spnTipo = (Spinner)findViewById(R.id.spnTipo);
        etNombre = (EditText)findViewById(R.id.etNombre);
        etEdad = (EditText)findViewById(R.id.etEdad);
        etDueno = (EditText)findViewById(R.id.etDueno);
        btnListar = (Button)findViewById(R.id.btnListar);


        cargaSpinner();


        btnFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });


        //txtEjemplo = (TextView)findViewById(R.id.txtEjemplo);

        btnVerImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarImagen(etUrlImagen.getText().toString());
                //txtEjemplo.getText();
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarMascota();
                Intent i = new Intent(FormularioActivity.this, ListaActivity.class);
                startActivity(i);
            }
        });

    }

    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                btnFecha.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);
    }

    private String formatDate(int value) {
        return (value > 9 ? value + "" : "0" + value);
    }


    private void cargaSpinner(){
        ArrayList<TipoAnimal> lista =  new ArrayList<TipoAnimal>();
        lista.add(new TipoAnimal("Gato"));
        lista.add(new TipoAnimal("Perro"));
        lista.add(new TipoAnimal("Cuyi"));
        lista.add(new TipoAnimal("Tortuga"));

        ArrayAdapter<TipoAnimal> adapter = new ArrayAdapter<TipoAnimal>(this,R.layout.support_simple_spinner_dropdown_item, lista);
        spnTipo.setAdapter(adapter);

    }


    private void guardarMascota(){
        Mascota nuevaMas = new Mascota();
        nuevaMas.setNombre(etNombre.getText().toString());
        nuevaMas.setTipo(spnTipo.getSelectedItem().toString());
        nuevaMas.setFecha_nac(btnFecha.getText().toString());
        nuevaMas.setEdad(etEdad.getText().toString());
        nuevaMas.setNombreDueno(etDueno.getText().toString());

        AlmacenMascotas.agregarFormulario(nuevaMas);
        Toast.makeText(this, "Formulario guardado correctamente", Toast.LENGTH_SHORT).show();


    }

    private void cargarImagen(String url){
        Picasso.with(this).load(url).into(ivPreview);
        //txtEjemplo.setText("funciona");
    }
}
