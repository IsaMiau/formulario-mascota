package com.example.iazoc.mascotas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.iazoc.mascotas.Clase.AdaptadorMascota;
import com.example.iazoc.mascotas.Clase.Mascota;

import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {

    private ListView lvMascotas;
    private ArrayList<Mascota> dataSource;
    private Button btnAgregar;
    private static ArrayList<Mascota> mas = AlmacenMascotas.getFormularios();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);

        btnAgregar = (Button)findViewById(R.id.btnAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ListaActivity.this, FormularioActivity.class);
                startActivity(i);
            }
        });


        lvMascotas = (ListView)findViewById(R.id.lvMascotas);
        AdaptadorMascota adaptador = new AdaptadorMascota(this, getDataSource());
        lvMascotas.setAdapter(adaptador);

        lvMascotas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListaActivity.this,
                        getDataSource().get(position).getNombre(), Toast.LENGTH_LONG).show();
            }
        });

    }

    public ArrayList<Mascota> getDataSource() {

        if(dataSource == null){
            dataSource = new ArrayList<>();

            for(int x = 0; x < mas.size(); x++){
                Mascota m = new Mascota(mas.get(x).getNombre(), mas.get(x).getTipo(), mas.get(x).getFecha_nac(), mas.get(x).getEdad(), mas.get(x).getNombreDueno(), mas.get(x).getUrlImagen());
                dataSource.add(m);
            }
        }

        return dataSource;
    }
}
